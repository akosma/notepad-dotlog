import * as vscode from 'vscode';
import * as os from 'os';

export function activate(context: vscode.ExtensionContext) {
	// Define the end-of-line sequence
	const EOL = os.EOL;
	const separator = EOL.repeat(2);

	// Formats the date passed as first argument following the formatting string passed as second argument
	function formatDateTime(dateTime: Date, format: string): string {
		const year = dateTime.getFullYear().toString();
		const month = (dateTime.getMonth() + 1).toString().padStart(2, '0');
		const day = dateTime.getDate().toString().padStart(2, '0');
		const hours = dateTime.getHours().toString().padStart(2, '0');
		const minutes = dateTime.getMinutes().toString().padStart(2, '0');
		const seconds = dateTime.getSeconds().toString().padStart(2, '0');

		return format
			.replace("YYYY", year)
			.replace("MM", month)
			.replace("DD", day)
			.replace("hh", hours)
			.replace("mm", minutes)
			.replace("ss", seconds);
	}

	// Function to get the date and time string based on user settings
	function getDateTimeString(): string {
		const config = vscode.workspace.getConfiguration('notepad-log');
		const format = config.get<string>('format', 'YYYY-MM-DD hh:mm:ss');
		return formatDateTime(new Date(), format);
	}

	// Function to insert date and time at the end of the document
	function insertDateTime(document: vscode.TextDocument, languageId: string) {
		const lastLine = document.lineAt(document.lineCount - 1);
		const endOfFilePosition = new vscode.Position(lastLine.range.end.line, lastLine.range.end.character);
		const dateTimeString = getDateTimeString();
		const edit = new vscode.WorkspaceEdit();
		let text = `${separator}${dateTimeString}${separator}`;
		switch (languageId)
		{
			case 'markdown':
				text = `${separator}## ${dateTimeString}${separator}`;
				break;

			case 'asciidoc':
				text = `${separator}== ${dateTimeString}${separator}`
				break;
		}
		edit.insert(document.uri, endOfFilePosition, text);
		vscode.workspace.applyEdit(edit);
	}

	// Function to handle opening of text documents
	function handleTextDocumentOpen(document: vscode.TextDocument) {
		const languageId = document.languageId;
		const isPlainText = languageId === 'plaintext' || languageId === 'log' || languageId === 'markdown' || languageId === 'asciidoc';
		if (isPlainText) {
			const firstLine = document.lineAt(0).text.trim();
			if (firstLine === '.LOG') {
				insertDateTime(document, languageId);
			}
		}
	}

	// Apply extension to already open text documents
	vscode.workspace.textDocuments.forEach(handleTextDocumentOpen);

	// Apply extension to newly opened documents
	let disposable = vscode.workspace.onDidOpenTextDocument(handleTextDocumentOpen);
	context.subscriptions.push(disposable);
}
