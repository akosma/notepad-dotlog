# Notepad .LOG Visual Studio Code Extension

This Visual Studio Code extension provides functionality available in Windows Notepad since at least Windows 3.1: the automatic creation of journal entries by humans on specific files with a specific first line: `.LOG`.

Here's the [official Microsoft Support page](https://support.microsoft.com/en-us/topic/how-to-use-notepad-to-create-a-log-file-dd228763-76de-a7a7-952b-d5ae203c4e12) explaining this feature.

## Features

This extension watches files of the following types:

- Plain Text
- Log
- Markdown
- AsciiDoc

If any file of this type starts with the text `.LOG`, this extension automatically inserts the current date and time.

In the case of Markdown and AsciiDoc, the date and time are inserted as second-level titles.

See the files in the `examples` repository.

## Settings

This extension contributes the following settings:

* `notepad-log.format`: The format used to display the date and time on the journal file. The default value is `'YYYY-MM-DD hh:mm:ss'`.

See the `.vscode/settings.json` file in this repository for an example.
